# SDP Industrial Day 2020: Website Tutorials

- [Your URL](#your-url)
- [Using the Web IDE](#using-the-web-ide)
  * [Saving in the Web IDE](#saving-in-the-web-ide)
- [Editing Pages](#editing-pages)
  * [Using Markdown](#using-markdown)
    + [Cheatsheet](#cheatsheet)
    + [Adding Images](#adding-images)
    + [Adding videos](#adding-videos)
      - [Via YouTube Link](#via-youtube-link)
      - [Embed Videos from your Repo](#embed-videos-from-your-repo)
    + [Previewing your Page](#previewing-your-page)
  * [Uploading Files](#uploading-files)
  * [Moving Files](#moving-files)
  * [Editing the Config File](#editing-the-config-file)
    + [Editing Title and Subtitle](#editing-title-and-subtitle)
    + [Adding Your Logo](#adding-your-logo)
    + [Editing Menu Links](#editing-menu-links)
  * [Using html](#using-html)
- [Building Your Site](#building-your-site)
- [Using Your Own Site](#using-your-own-site)
- [Support](#support)
- [Troubleshooting](#troubleshooting)


# Your URL

Welcome to your group's website for the SDP 2020 Industrial Day. 

Your site will be built in this private repository and can be viewed publically via gitlab pages here: 

`https://sdp2020.gitlab.io/group<group_number>/` 

Where `<group_number>` is replaced with the number of your group.


# Using the Web IDE

Gitlab contains a useful `Web IDE` tool which allows you to edit the contents of the site without cloning it. We recommend using this tool for ease of use. 

Click on the `Web IDE` button at the top of this page to start the IDE. You will then be taken to a file explorer, where you can edit the contents of files.

## Saving in the Web IDE

To save your changes in the web IDE, you have to push changes to the repo:
* Click the `commit` button at the bottom left of the screen
* Add a short message describing the changes you've made
* Commit your changes either to a new branch or the master branch (not recommended)
    * Create a pull request to merge your changes to master when you're ready 


# Editing Pages

The information for each page on your site is stored in the `content` folder of the repo.

Each page is built from a markdown file with the same name. To edit the contents of a page, you will edit the corresponding `.md` file using the WebIDE

## Using Markdown

Markdown is an easy to use language for creating clean, simple documents. We chose mardown because it is simple to use for those who have not spent any time developing websites. 

### Cheatsheet

Almost everything you need to know about markdown can be found in this [cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

Anything that can't be found here can be easily found with a simple search.

**Tip:** If you have any issues, the first thing to check is usually that there is a space between the markdown command and the start of your text.

### Adding Images

The images for your repo should be stored in the `/contents/images/` folder, otherwise they might not be found during building.

Adding images via markdown is simple, but if you need to adjust its size it can be tricky. If you need to do this, instead use html commands

```html
<img src="/images/<filename>" alt="alternative_text" width="100" height="100"/>
```

where `<filename>` is replaced by the name of your image and `"alternative_text"` replaced by a short description of your image. 

The `width` and `height` parameters can be used to adjust the size of your images. The units of these commands are in pixels.

### Adding videos

Videos can be added via YouTube link, or by embeding a video which is stored in your repo

#### Via YouTube Link

If your video is hosted on YouTube, copy the embed link by clicking on the video, then `Get Embed Code`. Paste this code into the appropriate point of your markdown page. 

The code should look like this:

```html
<iframe width="764" height="430"
src="https://www.youtube.com/embed/_sBBaNYex3E" frameborder="0"
allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
</iframe>
```

You can adjust the width and height of the video on your page by adjusting the values in the code.

#### Embed Videos from your Repo 

Your own videos can be stored on this repo (dependant on size). They can be stored in `contents/videos/`

Use the code below to link the video:

```html
<video width="320" height="240" controls>
  <source src="/videos/<filename>" type="video/mp4">
</video>
```

Where the `<filename>` is replaced with the name of the file.

Again, adjust the width and height accordingly.

### Previewing your Page

You can preview how your markdown code looks before you build your site by clicking the `Preview Markdown` button at the top of the page when you're using the WebIDE. 

Click on the `edit` button to return to the previous screen.

## Uploading Files

To upload files in the WebIDE:

* click the `Upload File` icon at the navigation bar on the left OR click the same button in the context menu of any existing folder

## Moving Files

To move a file:

* click the context menu next to the file you want to move
* press `Rename/Move`
* add the desired path of the file as a prefix
* click `Rename File`

**Example:** if you have a file named `my_image.png` if your home directory and want to move it into the contents/images folder, you would change the name of the file from `my_image.png` to `contents/images/my_image.png`. Once the filename is confirmed, the file will move.

## Editing the Config File

By editing the `config.toml` file in your home directory, you can alter the title of your page, the logo etc. 

**WARNING** Be cafeful when editing this file, as changing the wrong parameter can cause errors during the build process.

### Editing Title and Subtitle

To change the title and subtitle of your page, edit the `title` and `subtitle` variables in the `config.toml` file.

### Adding Your Logo

By default, the logo at the top of the page is a placeholder. To add your own logo:

* upload the image file for your logo to `/content/images/` with an appropriate filename
* Open the `config.toml` file
* Edit the `logo` variable under `[Params]` to point to the filename given above

The template will try squash the logo into an appropriate size as much as possible, but you may need to tinker to get it to fit correctly

### Editing Menu Links

On your site, you have a drop down menu on the right hand side. Though we suggest you keep these pages as they are, you can edit this menu if needed in the `config.toml` file.

You can change the order of the pages by changing the weight values. 

You can build pages from different files by changing the `url`. Note that pages must be inside the `content` folder for this to work.

## Using html

HTML is much more versatile than markdown, but has a slightly steeper learning curve. If members of your group prefer to use html this can be added to markdown files as desired. 

Page links can also be redirected to point to `.html` files if needed, but this has not been tested.

An example of using HTML code can be found in `content/budget.md`.

# Building Your Site

None of the changes you make to your site will be public until you run the build pipeline. 

To run the build pipeline:

* exit the Web IDE if you're in it (click the project name at the top left of the page)
* Click the `CI/CD` button on the left of the page
* Click `Run Pipeline` at the top right of the page
* Click `Run Pipeline` again in the centre of the page

This will start the build process and show a progress bar.

Once the build has been completed it can take up to a couple of minutes to update your site, but after that you should be able to see the updated version of your site at the URL at the top of this page.

The build pipeline is defined in the `.gitlab-ci.yml` file. Do not make changes to this file unless you know what you're doing. 

# Using Your Own Site

Links to each of the groups websites will be linked from the main SDP page. 

If your group chooses to build their own website and not use this template, you can do any of the following:

* Push your site to this repo and use the repo URL
* Send the URL to your external site to the TAs. This can then be added to the main industrial day landing page

# Support

You are likely to encounter technical/build issues with your site. 

The first port of call should always be searching for solutions within your group, then consulting help sites such as StackOverflow or search engines. Feel free to contact the Experts or TAs (primarily Traiko and Chris), but given the difficulties of remote debugging, please try to solve the issue as much as you can before contacting, as this can only slow down the process.

# Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
