---
title: System
subtitle: How does our System Work?
comments: false
---

## Methods

Explain the key methods of how you got your system to work

### System Diagrams

You should include images (where available) which illustrate the methods you used, how the system works etc. 

This can include system diagrams, images of your system in certain states etc.
