---
title: Team
subtitle: Meet the Team Members
comments: false
---

## Team Members and Roles

|   |    Name  |       Role      |
|:-:|:--------:|:---------------:|
| <img src="/images/placeholder_face.png" alt="person" width="100"/>  | Person 1 | Software Person |
| <img src="/images/placeholder_face.png" alt="person" width="100"/>  | Person 2 | Hardware Person |
| <img src="/images/placeholder_face.png" alt="person" width="100"/>  | Person 3 | Project Manager |

## Team Management

Use this section to demonstrate how your team was organised. How did you distribute your work? Which development tools did you use? Did you use git? Asana etc? This section might contain your Gantt chart or other similar diagrams. 
