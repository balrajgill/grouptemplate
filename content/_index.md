# Welcome 

[comment]: <> (This block adds links to your welcome page. We suggest keeping them there)
<link href="/styles/main.css" rel="stylesheet" type="text/css"/>
<div class="inpage-menu-container">
    <div><a href="/system">System</a></div>
    <div><a href="/howdoesitwork">How it Works</a></div>
    <div><a href="/evaluation">Evaluation</a></div>
    <div><a href="/budget">Budget</a></div>
    <div><a href="/team">Теаm</a></div>
</div>

This will be the landing page for your site. 

You should put the equivalent to your poster/leaflet here to market your product.

Here's a placeholder which you can replace with your image:

<img src="/images/sdp_banner_placeholder.png" alt="group_banner" width="600"/>
